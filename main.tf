data "template_file" "user_data" {
  template = var.cloud_init_user_data
}

data "template_file" "network_config" {
  template = var.cloud_init_network_config
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "${var.machine_name}-cloudinit.iso"
  user_data      = var.cloud_init_user_data != null ? data.template_file.user_data.rendered : null
  network_config = var.cloud_init_network_config != null ? data.template_file.network_config.rendered : null
}

resource "libvirt_domain" "machine" {
  name   = var.machine_name
  vcpu   = var.machine_vcpu
  memory = var.machine_memory * 1024

  cloudinit = var.cloud_init ? libvirt_cloudinit_disk.commoninit.id : null

  disk {
    volume_id = libvirt_volume.vol.id
    scsi      = "true"
  }

  network_interface {
    network_name   = var.machine_bridge
    mac            = var.machine_mac
    wait_for_lease = var.wait_for_lease
  }

  dynamic "filesystem" {
    for_each = var.shared_folders
    content {
      source   = filesystem.value.source
      target   = filesystem.key
      readonly = filesystem.value.readonly
    }
  }

  # fix growpart kernel panic
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  qemu_agent = var.qemu_agent
  autostart  = var.autostart
}
