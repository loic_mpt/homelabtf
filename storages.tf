resource "libvirt_volume" "base_vol" {
  name   = "${var.machine_name}-${var.machine_img}"
  source = "${var.imgs_location}/${var.machine_img}"
}

resource "libvirt_volume" "vol" {
  name           = "${var.machine_name}-vol.qcow2"
  base_volume_id = libvirt_volume.base_vol.id
  size           = var.machine_size != null ? var.machine_size * 1073741824 : null # Gb to Byte
}
