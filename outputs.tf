output "machine_name" {
  value = var.machine_name
}

output "machine_bridge" {
  value = var.machine_bridge
}

output "machine_vcpu" {
  value = var.machine_vcpu
}

output "machine_memory" {
  value = var.machine_memory
}

output "machine_img" {
  value = var.machine_img
}

output "network_interface" {
  value = libvirt_domain.machine.network_interface
}
