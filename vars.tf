variable "machine_name" {
  type    = string
  default = "default-name"
}

variable "machine_vcpu" {
  type    = number
  default = 1
}

variable "machine_memory" {
  type    = number
  default = 1
}

variable "machine_bridge" {
  type    = string
  default = "default"
}

variable "machine_img" {
  type    = string
  default = "debian-11-generic-amd64-daily.qcow2"
}

variable "machine_mac" {
  type        = string
  description = "AA:BB:CC:11:22:22" # Il est possible de jouer sur l'adresse MAC pour creer une adresse IP sur le DHCP
  default     = null
}

variable "machine_size" {
  type        = number
  default     = null # unit Gb
  description = "resize '/' if greater than base machine image"
  # manual resizefs
  # https://linuxhandbook.com/resize-lvm-partition/
}

variable "autostart" {
  type    = bool
  default = true
}

variable "cloud_init" {
  type    = bool
  default = true
}

variable "cloud_init_network_config" {
  type        = string
  description = "can be file or a script"
  default     = null
}

variable "cloud_init_user_data" {
  type        = string
  description = "can be file or a script"
  default     = null
}

variable "shared_folders" {
  type = map(object({
    source   = string
    readonly = bool
  }))
  description = "multiple folders shared by 9p host <-> guest"
  default     = {}
  # enable kernel module on guest and install all 9p deb with apt search
  # https://rabexc.org/posts/p9-setup-in-libvirt
}

variable "qemu_agent" {
  type    = bool
  default = false
}

variable "wait_for_lease" {
  type    = bool
  default = false
}

# host var

variable "URI_QEMU" {
  type        = string
  description = "var from env"
}

variable "imgs_location" {
  type    = string
  default = "/var/lib/libvirt/images"
  # default = "https://cloud.debian.org/images/cloud/bullseye/daily/latest" # Image debian toute fraiche
}

variable "pool_path" {
  type    = string
  default = "/var/lib/libvirt/images/pools"
}
