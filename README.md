# HOMELAB

> My homelab with terraform, libvirt

## Init

> add `security_driver = "none"` in `/etc/libvirt/qemu.conf`

```bash
# change server user and ip (remote)
export TF_VAR_URI_QEMU="qemu+ssh://toto@192.168.0.100/system"
# or (local)
export TF_VAR_URI_QEMU="qemu:///system"

terraform init

```

## Example of use

```t

module "homelabtf" {
  source = "git::https://gitlab.com/loic_mpt/homelabtf.git"

  URI_QEMU     = "qemu:///system"
  machine_name = "example"
}

```

### Cloud-init

```t

module "homelabtf" {
  source = "git::https://gitlab.com/loic_mpt/homelabtf.git"

  URI_QEMU     = "qemu:///system"
  machine_name = "example"

  cloud_init = true
  cloud_init_network_config = file("./config/network_config.yml")
  cloud_init_user_data = file("./config/user_data.yml")
  # shell script or cloud-config
  # https://cloudinit.readthedocs.io/en/latest/topics/examples.html
}

```

### Shared folders

#### Host

```bash
# root
d="/srv/test"
mkdir -p $d
chown libvirt-qemu:libvirt-qemu $d
chmod -R 770 $d

```

#### TF

```t

module "homelabtf" {
  source = "git::https://gitlab.com/loic_mpt/homelabtf.git"
  
  ...

  shared_folders = {
    "share1" = {
      source = "/srv/test"
      readonly = false
    }
  }
}

```

#### Guest

[p9 setup libvirt](https://rabexc.org/posts/p9-setup-in-libvirt)

```bash
d="/srv/test"
l="share1"
# test
sudo mount share1 $d -t 9p -o trans=virtio
# fstab
echo "$l $d 9p trans=virtio 0 0" >> /etc/fstab
```

### usb share

#### get infos

```bash

# lsusb -v
idVendor           0x1a86 QinHeng Electronics
idProduct          0x55d4 

```

#### create xml file

```xml
<!-- usb_device.xml -->
<hostdev mode='subsystem' type='usb' managed='yes'>
  <source>
    <vendor id='0x1a86'/>
    <product id='0x55d4'/>
  </source>
</hostdev>
```

#### attach device

```bash

virsh attach-device vm1 --file usb_device.xml --config --persistent
reboot 
virsh detach-device vm1 --file usb_device.xml
```

### resise fs lvm

> cloudinit can't resize lvm

```bash

fdisk /dev/sda << EOF
d
2
n
e



n


n
x
b


r
t

8e

w

EOF

apt install -y parted 
partprobe /dev/sda
pvresize /dev/sda5 

lvextend -L +3G /dev/mapper/debian--vg-tmp
lvextend -L +8G /dev/mapper/debian--vg-root
lvextend -l +100%FREE /dev/mapper/debian--vg-var

resize2fs /dev/mapper/debian--vg-tmp
resize2fs /dev/mapper/debian--vg-root
resize2fs /dev/mapper/debian--vg-var

```

## Example: building an Alpine linux image

### Download latest VIRTUAL Alipine

[here](https://www.alpinelinux.org/downloads/)

### Run it with virt-manager

### Configure Alipne

```bash

setup-alpine
reboot

```

### Configure Cloud-init

> uncomment the community repo in `/etc/apk/repositories`

```bash

apk update
apk add py3-pyserial py3-netifaces e2fsprogs-extra
apk add cloud-init
setup-cloud-init
poweroff

```
